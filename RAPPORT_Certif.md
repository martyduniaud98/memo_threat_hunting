# Threat Hunting MEMO

C'est le processus humain de recherche proactive de données et de découverte de cybermenaces

## Sommaire

- [Hunting](#hunting)
- [Incident Response Process (IR process)](#incident-response-process-ir-process)
- [Threat Hunting Teams](#threat-hunting-teams)
- [APT (Advanced Persistent Threat)](#apt-advanced-persistent-threat)
- [TTP (Tactics, Techniques and Procedures)](#ttp-tactics-techniques-and-procedures)
- [IOCs (Indicators of Compromise)](#iocs-indicators-of-compromise)
- [Pyramid of Pain](#pyramid-of-pain)
- [Cyber kill chain](#cyber-kill-chain)
- [Threat Hunting Mindset : Threat Intelligence](#threat-hunting-mindset--threat-intelligence)
- [Threat Hunting Mindset : Digital Forensics](#threat-hunting-mindset--digital-forensics)
- [Hunting Periods](#hunting-periods)
- [Reverse Engineering Binaries](#reverse-engineering-binaries)
- [Threat Sharing and exchanges](#threat-sharing-and-exchanges)
- [Tools for IOC](#tools-for-ioc)
- [Hunting Hypothesis and Methodology](#hunting-hypothesis-and-methodology)
- [Souligné lors du LAB1 Méthodologie: Hunting with IOCs](#soulign%C3%A9-lors-du-lab1-m%C3%A9thodologie-hunting-with-iocs)

<br>

## Hunting

- Est une stratégie basée sur l'offensive
- Oblige le hunter à penser comme un attaquant
- Requiert une solide compréhension pratique des cybermenaces et de la chaine de destruction des cybercriminels
- <U>Nécessite de connaitre son environnement </U>
- Est plus facile avec des données et ressources de qualités

<br>

## Incident Response Process (IR process)

![](img/IRprocess.png)

- <strong>Préparation</strong>, consiste à préparer l'organisation à gérer les incidents.
- <strong>Détection and analysis</strong>, l'équipe IR confirme si une violation a eu lieu. Ils analysent tout les symptomes réportés et confirment si la situation doit être classée comme incident ou non.
- <strong>Containment, Eradication and recovery </strong>, est l'endroit où l'équipe IR recueille des infos et créaient des signatures qui les aident à identifier chaque système compromis. Avec ces infos, des contre-mesures peuvent-être mises en place pour neutraliser l'attaquant et tenter de restaurer les systèmes/données à la normale.
- <strong>Post-incident activity</strong>, est la "leçon apprise" de l'incident. L'objectif est d'améliorer la posture de sécurité globale de l'organisation et d'assurer qu'un incident similaire ne se reproduira pas.

<br>

## Threat Hunting Teams

- La composition d'équipe de chasse la plus courament rencontrée :
  - <strong>Ad-hoc Hunter</strong> : une personne qui effectuent des recherches non planifiées pour trouver des menaces de sécurité et des vulnérabilités dans un système ou un réseau. Les Ad-hoc hunters peuvent être des employés de l'entreprise ou des consultants externes embauchés pour effectuer une évaluation de sécurité sur un système donné.
  - <strong>Analyst and Hunter</strong> : un expert en sécurité informatique qui analyse les journaux de sécurité et les données de télémétrie pour identifier les menaces potentielles. L'analyste utilise des techniques d'analyse avancées pour déterminer si une activité est malveillante ou non, puis utilise des outils de chasse aux menaces pour enquêter sur l'incident et identifier l'ampleur de la compromission.
  - <strong>Dedicated Hunting Team</strong> : un groupe d'experts en sécurité informatique chargés de la recherche active de menaces et de vulnérabilités dans un système ou un réseau. Ils utilisent une variété d'outils et de techniques pour détecter les activités malveillantes, y compris les signatures de malwares, les anomalies de trafic et les comportements suspects. Ils sont souvent employées par des organisations qui ont des niveaux de risque de sécurité élevés et qui ont besoin d'une surveillance proactive pour détecter les menaces de sécurité avant qu'elles ne causent des dommages.

<br>

## APT (Advanced Persistent Threat)

Une <strong>Advanced Persistent Threat</strong> est un type de piratage informatique furtif et continu, ciblant une entité spécifique.

Les groupes APT sont des groupes ou état-nation qui possèdent d'importantes ressources et infrastructures pour conduire à bien leurs activités malveillantes.

<br>

## TTP (Tactics, Techniques and Procedures)

- TTPs représente les méthodes ou signature de l'attaquand. Il s'agit de la manière dont l'attaquant va tenter d'atteindre ses objectifs.
  - <strong>Tactics</strong> : Les tactiques sont des stratégies globales utilisées pour atteindre un objectif. Ce sont les grandes étapes de l'attaque telles que la reconnaissance, l'exploitation, l'escalade de privilèges..
  - <strong>Techniques</strong> : Les techniques sont les méthodes spécifiques utilisées pour mettre en œuvre une tactique. Elles peuvent inclure l'utilisation d'outils spécifiques, la manipulation de code, l'exploitation de vulnérabilités..
  - <strong>Procedures</strong> : Les procédures sont les étapes spécifiques nécessaires pour exécuter une technique. Elles peuvent inclure la séquence de commandes à exécuter, les paramètres spécifiques à utiliser pour un outil, les étapes pour contourner les mécanismes de sécurité..

<br>

## IOCs (Indicators of Compromise)

- Les TTPs aident à identifier l'attaquant pour les futures attaques en créant des IOCs.
- IOCs sont des artefacts qui sont récoltés lors d'une intrusion active ou précédente qhe l'on utilise pour identifier un attaquant particulier. Ces artefacts inclus des hash MD5, des adresses IP, des noms d'EXE utilisés..

<br>

## Pyramid of Pain

![](img/pyramid.png)

Il ne s’agit pas vraiment d’un outil d’analyse à l’usage du renseignement sur les menaces (Threat Intelligence) mais plus une aide à la décision destinée aux équipes d'IR afin qu’elles tapent en priorité là où cela fera le plus de mal à l’attaquant.

Elle est divisée en strates, chacune d’entre elles étant associée à un type d’indicateurs et un niveau de douleur. Plus on monte dans ces strates, plus la douleur ressentie/infligée est élevée.

<br>

## Cyber kill chain

Exemple d'un modèle de CKC dans un scénario d'attaque simple.

![](img/ckc.png)

_les cinq premières étapes sont les <strong>intrusions stages</strong>, les deux dernière sont les <strong>active breach stages</strong>_

- <strong>C&C</strong> : est la phase de Commande et Controle (C2). Quand la machine de la victime appellera une adresse IP sur le domaine et fournira à l'attaquant un accès à distance en ligne de commande à la machine compromise.
- <strong>Action on objectives</strong> : C'est quand le but est atteint. Le but peut être l'exfiltration. c'est quand :
  - l'attaquant trouve les données recherchées et les extrait du système compromis.
  - ce que nous essayons de protéger est extrait du réseau. (Echec de la défense)

<br>

<u>Le but de la defense</u> est de bloquer la progression de l'attaquant dans la chaine de kill. Faire cela dans les premières étapes de la chaine de kill est toujours mieux.

<u>Le but du Hunter</u> est de détecter l'attaquant avant que son objectif ne soit atteint. Pas seulement lors de l'execution de code malveillant.

<br>

## The Diamond Model

Dans sa forme la plus simple, le modèle décrit qu'un attaquant déploie une capacité sur une certaine infra contre une victime :

![](img/diamond.jpg)

_Le modèle Diamant peut être utiliser en conjonction avec le modèle de la chaine de kill.
Rappel, le but est d'empêcher l'attaquant d'atteindre son objectif._

<br>

## Threat Hunting Mindset : Threat Intelligence

La définition simple de la <strong>Threat Intelligence</strong> est l'information sur les menaces qui est utilisée pour identifier, prioriser et réduire les risques de sécurité.

- On peut la diviser en 3 catégories :
  - <strong>Strategic</strong> : Qui, pourquoi et où
  - <strong>Tactical</strong> : Quoi et quand
  - <strong>Operational</strong> : Comment

<br>

## Threat Hunting Mindset : Digital Forensics

Ce hunter se concentrera principalement sur l'hôte, le réseau et la mémoire médico-légale dans sa chasse lors de la chasse à l'inconnu.

- Avec les sources de données disponibles, nous pouvons choisir d'effectuer des chasses de 2 manières distinctes :
  - Attack based hunting
  - Analytics-based hunting

_Attack based hunting :  
Dans la chasse basée sur les attaques, nous recherchons des preuves indiquant si une attaque spécifique s'est produite ou non dans l'environnement. "Est-ce que .... s'est produit dans mon réseau ?"_

_Analytics-based hunting :
Dans la chasse basée sur l'analyse, nous examinons un ensemble de données et essayons de voir si quelque chose se démarque. Il est donc crucial de savoir ce qui est normal.
"Est-ce que quelque chose dans .... données semble malveillant ?"_

<br>

## Hunting Periods

C'est la période de temps pendant laquelle nous allons collecter des données pour notre chasse.

- <strong>Point in Time</strong> : Détecte uniquement ce qui se passe sur un système à un moment donné. Forte probabilité de manquer des données volatiles de courte durée.
- <strong>Real Time</strong> : Détecte l'activité qui se produit en temps réel. Une configuration personnalisée pour la collecte
- <strong>Historic</strong> : Utilise des logs pour identifier les activités qui se sont produites dans le passé. La journalisation doit être configurée à l'avance.

<br>

## Reverse Engineering Binaries

Le chasseur peut également désosser les binaires, pour voir si le binaire est légitime ou malveillant, avec des logiciels comme IDA Pro, OllyDbg, Hopper, Radare2, Ghidra, Binary Ninja, etc.

<br>

## Threat Sharing and exchanges

- <strong>ISACs (Information Sharing and Analysis Centers)</strong> : Ce sont des organisations dirigés par leurs membres, qui fournissent des informations sur tous les risques et leurs mesures d'atténuation aux propriétaires et exploitants d'actif.
- <strong>US-CERT (United States Computer Emergency Readiness Team)</strong> : Ils répondent aux incidents majeurs, analyse les menaces et fournissent des informations critiques sur la cybersécurité.
- <strong>OTX (AlienVault's Open Threat Exchange)</strong> : C'est une communauté ouverte de renseignements sur les menaces.
- <strong>Threat Connect</strong> : Similaire a OTX -<strong>MISP (Malware Information Sharing Platform)</strong> : C'est une solution logicielle open source pour la collecte, le stockage, la distribution et le partage d'indicateurs de cybersécurité et de menaces concernant l'analyse des incidents de cybersécurité et l'analyse des logiciels malveillants.

<br>

## Tools for IOC

_Indicator Of Compromise (IOCs) sont des éléments de données médico-légales telles que des données trouvées dans des entrées de journal système ou des fichiers, qui identifient une activité potentiellement malveillante sur un système ou un réseau._

- <strong>OpenIOC</strong> développé par FireEye, fournit un format standad et des termes pour décrire les artefacts rencontrés au cours d'une enquête.
- <strong> IOC Editor</strong> est un outil gratuit qui fournit une interface pour gérer les données et manipuler les structures logiques d'IOC.
- <strong>Redline</strong> dans le cadre de la certification eCTHPv2, utilisé seulement pour rechercher des IOCs sur une machine.
- <strong>YARA</strong> est un outil destiné à (mais pas que) aider les chercheurs de logiciels malveillants à identifier et à classer les échantillons de logiciels malveillants.
- <strong>WinMD5</strong> est un outil petit et simple pour calculer les hash MD5.

<br>

## Hunting Hypothesis and Methodology

- Les 5 étapes :
  - Choisir une tactique et une technique
  - Identifier les procédures associés
  - Faire une simulation d'attaque
  - Identifier les preuves à recueillir
  - Définir la portée

## Souligné lors du LAB1 Méthodologie: Hunting with IOCs

- <strong>MITRE ATT&ACK</strong> -> MITRE's Adversary Tactics, Techniques, and Common Knowledge (ATT&CK) est un référentiel de connaissances sur les tactiques et techniques utilisées par les attaquants pour compromettre les systèmes.
- <strong>Data Governance (DG)</strong> -> est la gestion globale de la <strong>disponibilité</strong>, de l'<strong>utilisabilité</strong>, de l'<strong>intégrité</strong> et de la <strong>sécurité</strong> des données utilisées dans une entreprise.
<br>
